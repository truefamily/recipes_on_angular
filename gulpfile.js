var gulp = require('gulp');
var less = require('gulp-less');
var ts = require('gulp-typescript');
var browser_sync = require('browser-sync').create();
var del = require('del');

var paths = {
    html: ['partials/*.*', 'partials/includes/*.*', 'templates/*.*', 'index.html'],
    less: 'css/main.less',
    watchless: 'css/*.less',
    scripts: 'js/**/*.ts',
    jslibs: ['node_modules/toastr/build/toastr.min.js',
            'node_modules/bootstrap/dist/js/bootstrap.min.js',
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/angular/angular.js',
            'node_modules/angular-animate/angular-animate.js',
            'node_modules/angular-route/angular-route.js',
            'node_modules/angular-sanitize/angular-sanitize.js'],
    csslibs: ['node_modules/toastr/build/toastr.min.css',
            'node_modules/bootstrap/dist/css/bootstrap.min.css'],
    images: 'images/*.*'
};

gulp.task('build',
    [
        'less',
        'scripts',
        'jslibs',
        'csslibs',
        'images',
        'html',
        'watch',
        'browser_sync'
    ]);

// Not all tasks need to use streams
// A gulpfile is just another node program and you can use all packages available on npm
gulp.task('clean', function() {
    // You can use multiple globbing patterns as you would with `gulp.src`
    del(['build/**']);

});
/*
gulp.task('scripts', function() {
    gulp.src(paths.scripts)
        .pipe(gulp.dest('build/js'));
});
*/
gulp.task('scripts', function () {
    return gulp.src(paths.scripts)
        .pipe(ts({
            noImplicitAny: true,
            out: 'app2.js'
        }))
        .pipe(gulp.dest('build/js'));
});

gulp.task('jslibs', function () {
    return gulp.src(paths.jslibs)
        .pipe(gulp.dest('build/js/libs'));
});

gulp.task('csslibs', function () {
    return gulp.src(paths.csslibs)
        .pipe(gulp.dest('build/css/libs'));
});

gulp.task('less', function () {
    gulp.src(paths.less)
        .pipe(less())
        .pipe(gulp.dest('build/css'));
});

gulp.task('images', ['clean'], function() {
    gulp.src(paths.images)
        .pipe(gulp.dest('build/images'));
});

gulp.task('html', function() {
    gulp.src(paths.html)
        .pipe(gulp.dest('build'));
});


gulp.task('watch', function() {
    gulp.watch(paths.watchless, ['less']);
    gulp.watch(paths.scripts, ['scripts']);
    gulp.watch(paths.images, ['images']);
    gulp.watch(paths.html, ['html']);
});

gulp.task('browser_sync', function() {
    browser_sync.init({
        server: {
            baseDir: "./build/",
            index: "index.html"
        }
    });
});

