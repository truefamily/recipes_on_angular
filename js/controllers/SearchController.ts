module Application.Controllers {
    import Recipe = Application.Models.Recipe;

    export class SearchController {

        public static $inject = [
            '$scope',
            '$location',
            'Service',
            'SearchService'
        ];

        constructor(
            private $scope: Application.Interfaces.IRecipesScope,
            private $location: ng.ILocationService,
            private service: Application.Services.Service,
            private searchService: Application.Services.SearchService
        )
        {
            $scope.recipes = searchService.getRecipes();
        }

        search(searchWord: string): void {
            var searchService = this.searchService;
            var location = this.$location;
            this.service.searchByTag(searchWord).then(function(recipes: Recipe[]) {
                searchService.addRecipes(recipes);
                location.path("/result");
            });
        }
    }
}