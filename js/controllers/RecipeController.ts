module Application.Controllers {
    import Recipe = Application.Models.Recipe;
    import IRecipeScope = Application.Interfaces.IRecipeScope;

    export class RecipeController {

        public static $inject = [
            '$scope',
            '$routeParams',
            'Service'
        ];

        constructor(
            private $scope: Application.Interfaces.IRecipeScope,
            private $routeParams: Application.Interfaces.IRecipeRoute,
            private service: Application.Services.Service
        )
        {
            var recipeId = $routeParams.recipeId;
            service.getRecipe(recipeId).then(function(recipe: Recipe) {
                $scope.recipe = recipe;
            });
        }
    }
}
