module Application.Controllers {
    import Recipe = Application.Models.Recipe;
    export class ApprovesController {

        public static $inject = [
            '$scope',
            '$location',
            'Service'
        ];

        constructor(
            private $scope: Application.Interfaces.IApproveRecipesScope,
            private $location: ng.ILocationService,
            private service: Application.Services.Service)
        {
            this.getRecipes();
        }

        private getRecipes() : void {
            var scope = this.$scope;
            this.service.getRecipesToApprove().then(function(recipes: Recipe[]) {
                scope.recipes = recipes;
            });
        }

        approveRecipe(recipeId: number) : void {
            var scope = this.$scope;
            this.service.approveRecipe(recipeId).then(function(status: string) {
                scope.status = status;
                if(status === 'true') {
                    toastr.success('Рецепт одобрен');
                } else {
                    toastr.success('Не удалось одобрить рецепт');
                }

            });
            this.getRecipes();
        }

        rejectRecipe(recipeId: number) : void {
            var scope = this.$scope;
            this.service.rejectRecipe(recipeId).then(function(status: string) {
                scope.status = status;
                if(status === 'true') {
                    toastr.success('Рецепт отклонен');
                } else {
                    toastr.success('Не удалось одобрить рецепт');
                }
            });
            this.getRecipes();
        }
    }
}