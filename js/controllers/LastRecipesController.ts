module Application.Controllers {
    import Recipe = Application.Models.Recipe;

    export class LastRecipesController {

        public static $inject = [
            '$scope',
            'Service'
        ];

        constructor(
            private $scope: Application.Interfaces.IRecipesScope,
            private service: Application.Services.Service
        )
        {
            service.getLastRecipes().then(function(recipes: Recipe[]) {
                $scope.recipes = recipes;
            });
        }
    }
}