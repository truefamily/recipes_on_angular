module Application.Controllers {
    import Recipe = Application.Models.Recipe;
    import IRecipeScope = Application.Interfaces.IRecipeScope;
    import Tag = Application.Models.Tag;
    import Ingridient = Application.Models.Ingridient;
    import Measure = Application.Models.Measure;

    export class CommonRecipeController {

        public static $inject = [
            '$scope',
            'Service'
        ];

        constructor(
            protected $scope: IRecipeScope,
            protected service: Application.Services.Service)
        {
            service.getMeasures().then(function(measures: Measure[]) {
                $scope.measures = measures;
            });
        }

        protected resetForm(scope: IRecipeScope) : void {
            scope.recipe=undefined;
            scope.newTag=undefined;
            scope.newIngridient=undefined;
        }

        protected saveRecipe(recipe: Recipe) : void {
            if (!this.isRecipeValid()) return;
            var thisScope = this;
            var scope = this.$scope;
            this.service.addRecipe(recipe).then(function(recipeId: number) {
                scope.recipe.id = recipeId;
                toastr.success('Рецепт сохранен');
                thisScope.resetForm(scope);
            });
        }

        protected appendTag() : void {
            var tag = this.$scope.newTag;
            if (tag === undefined || tag.name === undefined || tag.name.length == 0) {
                return;
            }
            if (this.$scope.recipe.tags === undefined) {
                this.$scope.recipe.tags = [];
            }
            this.$scope.recipe.tags.push(tag);
            this.$scope.newTag = undefined;
        }

        protected appendIngr() : void {
            var ingridient = this.$scope.newIngridient;
            if (ingridient === undefined || ingridient.name === undefined || ingridient.name.length == 0 || ingridient.volume <= 0 || ingridient.measure === undefined) {
                return;
            }
            if (this.$scope.recipe.ingridients === undefined) {
                this.$scope.recipe.ingridients = [];
            }
            this.$scope.recipe.ingridients.push(ingridient);
            this.$scope.newIngridient = undefined;

        }

        protected removeTag(tagName: string) : void {
            this.$scope.recipe.tags = this.$scope.recipe.tags.filter(function (el: Tag) : boolean {
                return el.name !== tagName;
            });
        }

        protected removeIngr(ingr: Ingridient) : void {
            this.$scope.recipe.ingridients = this.$scope.recipe.ingridients.filter(function (el:Ingridient):boolean {
                return el.name !== ingr.name
                    || el.volume !== ingr.volume
                    || el.measure.id !== ingr.measure.id;
            });
        }

        protected isRecipeValid(): boolean {
            var recipe = this.$scope.recipe;
            if(recipe.title === undefined || !recipe.title) {
                toastr.warning("Не заполнено название рецепта", "Рецепт не сохранен");
                return false;
            }
            if(recipe.tags === undefined || recipe.tags.length < 1) {
                toastr.warning("Добавьте хотя бы один тег", "Рецепт не сохранен");
                return false;
            }
            if(recipe.ingridients === undefined || recipe.ingridients.length < 1) {
                toastr.warning("Добавьте хотя бы один ингридиент", "Рецепт не сохранен");
                return false;
            }
            if(recipe.content === undefined || !recipe.content) {
                toastr.warning("Не заполнен способ приготовления", "Рецепт не сохранен");
                return false;
            }
            return true;
        }
    }
}