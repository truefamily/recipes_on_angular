/// <reference path='CommonRecipeController.ts' />
module Application.Controllers {
    import Recipe = Application.Models.Recipe;
    import IRecipeScope = Application.Interfaces.IRecipeScope;
    import Tag = Application.Models.Tag;
    import Ingridient = Application.Models.Ingridient;
    import Measure = Application.Models.Measure;

    export class AddRecipeController extends CommonRecipeController {

        public static $inject = [
            '$scope',
            'Service'
        ];

        constructor(
            protected $scope: IRecipeScope,
            protected service: Application.Services.Service)
        {
            super($scope, service);
        }

        saveRecipe(recipe: Recipe) : void {
            if (!this.isRecipeValid()) return;
            var thisScope = this;
            var scope = this.$scope;
            this.service.addRecipe(recipe).then(function(recipeId: number) {
                scope.recipe.id = recipeId;
                toastr.success('Рецепт сохранен');
                thisScope.resetForm(scope);
            });
        }
    }
}