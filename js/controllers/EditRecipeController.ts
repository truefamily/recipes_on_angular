/// <reference path='CommonRecipeController.ts' />
module Application.Controllers {
    import Recipe = Application.Models.Recipe;
    import IRecipeScope = Application.Interfaces.IRecipeScope;
    import Tag = Application.Models.Tag;
    import Ingridient = Application.Models.Ingridient;
    import Measure = Application.Models.Measure;

    export class EditRecipeController extends CommonRecipeController {

        private static loc: ng.ILocationService;

        public static $inject = [
            '$scope',
            '$routeParams',
            '$location',
            'Service'
        ];

        constructor(
            protected $scope: IRecipeScope,
            protected $routeParams: Application.Interfaces.IRecipeRoute,
            protected $location: ng.ILocationService,
            protected service: Application.Services.Service)
        {
            super($scope, service);
            EditRecipeController.loc = $location;
            var recipeId = $routeParams.recipeId;
            service.getRecipe(recipeId).then(function(recipe: Recipe) {
                $scope.recipe = recipe;
            });
        }

        saveRecipe(recipe: Recipe) : void {
            if (!this.isRecipeValid()) return;
            this.service.editRecipe(recipe).then(function(status: string) {
                if(status === 'true') {
                    toastr.success('Изменения сохранены');
                    EditRecipeController.loc.path('/home');
                } else {
                    toastr.warning('Ошибка при сохранении рецепта.');
                }
            });
        }

        deleteRecipe(recipeId: number) : void {
            this.service.deleteRecipe(recipeId).then(function(status: string) {
                if(status === 'true') {
                    toastr.success('Рецепт успшено удален');
                    EditRecipeController.loc.path('/home');
                } else {
                    toastr.warning('Ошибка при удалении рецепта.');
                }
            })
        }
    }
}