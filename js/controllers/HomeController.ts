module Application.Controllers {
    import Recipe = Application.Models.Recipe;

    export class HomeController {

        public static $inject = [
            '$scope',
            '$location',
            'Service'
        ];

        constructor(
            private $scope: Application.Interfaces.IRecipesScope,
            private $location: ng.ILocationService,
            private service: Application.Services.Service
        )
        {
            service.getRecipes().then(function(recipes: Recipe[]) {
                $scope.recipes = recipes;
            });
            this.getRecipeCount();
        }

        getRecipeCount() : void {
            var scope = this.$scope;
            this.service.getRecipeCount().then(function(count: number) {
                scope.count=count;
            });
        }
    }
}