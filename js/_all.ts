/// <reference path='libs/jquery/jquery.d.ts' />
/// <reference path='libs/angularjs/angular.d.ts' />
/// <reference path='libs/bootstrap/bootstrap.d.ts' />
/// <reference path='libs/toastr/toastr.d.ts' />

/// <reference path='services/Service.ts' />
/// <reference path='services/SearchService.ts' />

/// <reference path='controllers/HomeController.ts' />
/// <reference path='controllers/RecipeController.ts' />
/// <reference path='controllers/ApprovesController.ts' />
/// <reference path='controllers/AddRecipeController.ts' />
/// <reference path='controllers/EditRecipeController.ts' />
/// <reference path='controllers/LastRecipesController.ts' />
/// <reference path='controllers/SearchController.ts' />

/// <reference path='models/Tag.ts' />
/// <reference path='models/Recipe.ts' />

/// <reference path='Application.ts' />
