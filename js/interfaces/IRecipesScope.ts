/// <reference path='../_all.ts' />

module Application.Interfaces {
    import Recipe = Application.Models.Recipe;
    export interface IRecipesScope extends ng.IScope {
        recipes: Recipe[];
        count: number;
    }
}