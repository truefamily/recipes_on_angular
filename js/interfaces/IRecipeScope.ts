/// <reference path='../_all.ts' />

module Application.Interfaces {
    import Recipe = Application.Models.Recipe;
    import Tag = Application.Models.Tag;
    import Ingridient = Application.Models.Ingridient;
    import Measure = Application.Models.Measure;
    export interface IRecipeScope extends ng.IScope {
        recipe: Recipe;
        newTag: Tag;
        newIngridient: Ingridient;
        measures: Measure[];
    }
}