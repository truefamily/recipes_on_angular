/// <reference path='../_all.ts' />

module Application.Interfaces {
    import Recipe = Application.Models.Recipe;
    export interface IRecipeRoute extends ng.route.IRouteParamsService {
        /* must match the route param name */
        recipeId: number;
    }
}