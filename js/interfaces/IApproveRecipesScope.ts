/// <reference path='../_all.ts' />

module Application.Interfaces {
    import Recipe = Application.Models.Recipe;
    export interface IApproveRecipesScope extends ng.IScope {
        recipes: Recipe[];
        status: string;
    }
}