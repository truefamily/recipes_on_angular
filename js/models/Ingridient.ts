/// <reference path='../_all.ts' />

module Application.Models {
    'use strict';

    export class Ingridient {
        constructor(
            public id:number,
            public name:string,
            public volume:number,
            public measure:Measure
        )
        {}
    }
}
