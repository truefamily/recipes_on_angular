/// <reference path='../_all.ts' />

module Application.Models {
    'use strict';

    export class Tag {
        constructor(
            public id: number,
            public name: string
        ) { }
    }
}