/// <reference path='../_all.ts' />

module Application.Models {
    'use strict';

    export class Recipe {
        constructor(
            public id: number,
            public userId: number,
            public timestamp: string,
            public title: string,
            public content: string,
            public published: boolean,
            public rejected: boolean,
            public moderId: number,
            public difficult: number,
            public image: string,
            public whatches: number,
            public workTime: number,
            public kitchen: string,
            public isDiet: boolean,
            public tags: Tag[],
            public ingridients: Ingridient[]
        ) { }
    }
}