/// <reference path='../_all.ts' />

module Application.Models {
    'use strict';

    export class Comment {
        constructor(
            public id: number,
            public authorId: number,
            public content: string,
            public date: string
        ) { }
    }
}