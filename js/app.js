/**
 * Main AngularJS Web Application
 */
var app = angular.module('recipeApp', [
  'ngRoute', 'ngSanitize'
]);

// Наш API
var HOST = "http://silakov.xyz";
var API_RECIPE = HOST + "/recipe/";
var API_RECIPES = HOST + "/recipes";
var API_RECIPES_TO_APPROVE = HOST + "/recipesToApprove";
var API_APPROVE = HOST + "/approve";
var API_REJECT = HOST + "/reject";

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    .when("/", {templateUrl: "home.html", controller: "HomeCtrl"})
    .when("/home", {templateUrl: "home.html", controller: "HomeCtrl"})
    .when("/recipes/:recipeId", {templateUrl: "recipe.html", controller: "RecipeCtrl"})
    .when("/approves", {templateUrl: "approves.html", controller: "ApproveRecipesCtrl"})
    .when("/add", {templateUrl: "addRecipe.html", controller: "PageCtrl"})
    .otherwise({ redirectTo: '/404'});
}]);

app.controller('HomeCtrl', function ( $scope/*, $location, $http */) {
});

app.controller('RecipeCtrl', function ($scope, $routeParams) {
    $.ajax({
        url: API_RECIPE + $routeParams.recipeId,
        async: false,
        dataType: 'json',
        success: function(data) {
            $scope.recipe = data;
        }
    });
});

app.controller('RecipesCtrl', function ($scope) {
    $.ajax({
        url: API_RECIPES,
        async: false,
        dataType: 'json',
        success: function(data) {
            $scope.recipes = data;
        }
    });
});

app.controller('ApproveRecipesCtrl', function ($scope) {
    var getRecipes = function() {
        $.ajax({
            url: API_RECIPES_TO_APPROVE,
            async: false,
            dataType: 'json',
            success: function(data) {
                $scope.recipes = data;
            }
        });
    };

    console.log("ApproveRecipesCtrl Controller reporting for duty.");
    getRecipes();

    $scope.approve = function(id) {
        $.ajax({
            url: API_APPROVE + "/" + id,
            async: false,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                getRecipes();
            }
        });
    };

    $scope.reject = function(id) {
        $.ajax({
            url: API_REJECT + "/" + id,
            async: false,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                getRecipes();
            }
        });
    };

});

app.controller('AddRecipeCtrl', ['$scope', function($scope) {
    $scope.master = {};
    $scope.update = function(recipe) {
        $.ajax({
            url: API_RECIPES,
            async: false,
            type: "POST",
            dataType: 'json',
            data: JSON.stringify(recipe),
            success: function(data) {
                $scope.reset();
                $('#smallModal').find('span').text('Рецепт успешно добавлен!');
                $('#smallModal').modal('show');
            }
        });
    };

    $scope.reset = function(form) {
        if (form) {
            form.$setPristine();
            form.$setUntouched();
        }
        $scope.recipe = angular.copy($scope.master);
    };

    $scope.reset();
}]);

/**
 * Controls the Blog
 */
app.controller('BlogCtrl', function (/* $scope, $location, $http */) {
  console.log("Blog Controller reporting for duty.");
});

/**
 * Controls all other Pages
 */
app.controller('PageCtrl', function (/* $scope, $location, $http */) {
  console.log("Page Controller reporting for duty.");
});