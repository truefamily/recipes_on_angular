/// <reference path='_all.ts' />

/**
 * The main TodoMVC app module.
 *
 * @type {angular.Module}
 */
module Application {
    import RecipeController = Application.Controllers.RecipeController;
    import HomeController = Application.Controllers.HomeController;
    import ApprovesController = Application.Controllers.ApprovesController;
    import AddRecipeController = Application.Controllers.AddRecipeController;
    import EditRecipeController = Application.Controllers.EditRecipeController;
    import LastRecipesController = Application.Controllers.LastRecipesController;
    import SearchController = Application.Controllers.SearchController;
    'use strict';

    var recipeApp = angular.module('recipeApp', ['ngRoute'])
        .factory('SearchService', () => {return new Application.Services.SearchService();})
        .service('Service', Application.Services.Service)
        .controller('HomeController', HomeController)
        .controller('RecipeController', RecipeController)
        .controller('ApprovesController', ApprovesController)
        .controller('AddRecipeController', AddRecipeController)
        .controller('EditRecipeController', EditRecipeController)
        .controller('LastRecipesController', LastRecipesController)
        .controller('SearchController', SearchController)
        .config(['$routeProvider', '$httpProvider', function($routeProvider: ng.route.IRouteProvider,
                                                             $httpProvider: any) {
            $httpProvider.defaults.headers.common = {};
            $httpProvider.defaults.headers.post = {};
            $httpProvider.defaults.headers.get = {};
            $routeProvider
                .when('/', {controller: 'HomeController', templateUrl: 'home.html', controllerAs: 'ctrl'})
                .when('/home', {controller: 'HomeController', templateUrl: 'home.html', controllerAs: 'ctrl'})
                .when('/recipes/:recipeId', {
                    controller: 'RecipeController',
                    templateUrl: "recipe.html",
                    controllerAs: 'ctrl'
                })
                .when("/approves", {
                    controller: "ApprovesController",
                    templateUrl: "approves.html",
                    controllerAs: 'ctrl'
                })
                .when("/add", {
                    //controller: "AddRecipeController",
                    templateUrl: "addRecipe.html"
                    //controllerAs: 'ctrl'
                })
                .when("/edit/:recipeId", {
                    controller: "EditRecipeController",
                    templateUrl: "editRecipe.html",
                    controllerAs: 'ctrl'
                })
                .when("/result", {
                    controller: "SearchController",
                    templateUrl: "search.html",
                    controllerAs: 'ctrl'
                })
                .otherwise({redirectTo: "/404", templateUrl: "404.html"});
        }]);
}