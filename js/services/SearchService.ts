module Application.Services {
    import Recipe = Application.Models.Recipe;
    import Measure = Application.Models.Measure;
    export class SearchService {
        searchedRecipes: Recipe[];

        public addRecipes(recipes: Recipe[]): void {
            this.searchedRecipes = recipes;
        }

        public getRecipes(): Recipe[] {
            return this.searchedRecipes;
        }

    }
}