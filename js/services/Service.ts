module Application.Services {

    import Recipe = Application.Models.Recipe;
    import IRecipesScope = Application.Interfaces.IRecipesScope;
    import Measure = Application.Models.Measure;
    export class Service {
        //TODO Внимание, теперь вместо silakov.xyz указываем silakov.xyz/api !!!!
        HOST = "http://silakov.xyz/api";
        //DEBUG_MODE:
        //HOST = "http://192.168.1.12";

        API_RECIPE = this.HOST + "/recipe/";
        API_RECIPES = this.HOST + "/recipes";
        API_RECIPE_COUNT = this.HOST + "/recipeCount";
        API_SAVE_RECIPE = this.HOST + "/save";
        API_EDIT_RECIPE = this.HOST + "/edit";
        API_RECIPES_TO_APPROVE = this.HOST + "/recipesToApprove";
        API_APPROVE = this.HOST + "/approve";
        API_REJECT = this.HOST + "/reject";
        API_DELETE_RECIPE = this.HOST + "/reject/";
        API_MEASURES = this.HOST + "/measures";
        API_LAST_RECIPES = this.HOST + "/lastRecipes/7";
        API_SEARCH = this.HOST + "/searchByTag";

        constructor(private $http: ng.IHttpService, private $q : ng.IQService) {
        }

        // Получить рецепт по id
        getRecipe(id: number): ng.IPromise<Recipe> {
            var deferred = this.$q.defer();
            this.$http.get(this.API_RECIPE + id).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
            });
            return deferred.promise;
        }

        // Получить опубликованные и одобренные рецепты
        getRecipes(): ng.IPromise<Recipe[]> {
            var deferred = this.$q.defer();
            this.$http.get(this.API_RECIPES).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
            });
            return deferred.promise;
        }

        searchByTag(searchWord: string): ng.IPromise<Recipe[]> {
            var deferred = this.$q.defer();
            this.$http.get(this.API_SEARCH + "?q=" + searchWord).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
            });
            return deferred.promise;
        }

        // Получить опубликованные и одобренные рецепты
        getLastRecipes(): ng.IPromise<Recipe[]> {
            var deferred = this.$q.defer();
            this.$http.get(this.API_LAST_RECIPES).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
            });
            return deferred.promise;
        }

        // Получить рецепты, которые требуют одобрения
        getRecipesToApprove(): ng.IPromise<Recipe[]> {
            var deferred = this.$q.defer();
            this.$http.get(this.API_RECIPES_TO_APPROVE).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
            });
            return deferred.promise;
        }

        // Получить варианты измерений
        getMeasures(): ng.IPromise<Measure[]> {
            var deferred = this.$q.defer();
            this.$http.get(this.API_MEASURES).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
            });
            return deferred.promise;
        }

        // Одобрить рецепт
        approveRecipe(id: number): ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post(this.API_APPROVE + "/" + id, null).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
            });
            return deferred.promise;
        }

        // Отклонить рецепт
        rejectRecipe(id: number): ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post(this.API_REJECT + "/" + id, null).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
            });
            return deferred.promise;
        }

        // Сохранить рецепт
        addRecipe(recipe: Recipe): ng.IPromise<number> {
            var deferred = this.$q.defer();
            this.$http.post(this.API_SAVE_RECIPE, recipe).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
            });
            return deferred.promise;
        }

        // Обновить данные рецепта
        editRecipe(recipe: Recipe): ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post(this.API_EDIT_RECIPE, recipe).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
                toastr.error('Ошибка при сохранении рецепта');
            });
            return deferred.promise;
        }

        // Удалить рецепт
        deleteRecipe(id: number): ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post(this.API_DELETE_RECIPE + id, null).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
            });
            return deferred.promise;
        }

        getRecipeCount(): ng.IPromise<number> {
            var deferred = this.$q.defer();
            this.$http.get(this.API_RECIPE_COUNT).then(response => {
                deferred.resolve(response.data);
            }).catch( reason => {
                deferred.reject(reason);
            });
            return deferred.promise;
        }
    }
}